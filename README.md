# macros.py

Write a macro file from text:

``` bash
./macros.py --text "hello, world" --name "test" --filename "test.mac"
```