#! /usr/bin/env python3

import argparse
import pathlib
import configparser
from collections import OrderedDict

MACRO_FILE_VERSION=0

class KeyStroke():    
    def __init__(self, keycode: int, shift_pressed: bool):
        self.keycode = keycode
        self.shift_pressed = shift_pressed

down=0
up=1

key_stroke_dict = {
    "A" : KeyStroke(65, True),
    "B" : KeyStroke(66, True),
    "C" : KeyStroke(67, True),
    "D" : KeyStroke(68, True),
    "E" : KeyStroke(69, True),
    "F" : KeyStroke(70, True),
    "G" : KeyStroke(71, True),
    "H" : KeyStroke(72, True),
    "I" : KeyStroke(73, True),
    "J" : KeyStroke(74, True),
    "K" : KeyStroke(75, True),
    "L" : KeyStroke(76, True),
    "M" : KeyStroke(77, True),
    "N" : KeyStroke(78, True),
    "O" : KeyStroke(79, True),
    "P" : KeyStroke(80, True),
    "Q" : KeyStroke(81, True),
    "R" : KeyStroke(82, True),
    "S" : KeyStroke(83, True),
    "T" : KeyStroke(84, True),
    "U" : KeyStroke(85, True),
    "V" : KeyStroke(86, True),
    "W" : KeyStroke(87, True),
    "X" : KeyStroke(88, True),
    "Y" : KeyStroke(89, True),
    "Z" : KeyStroke(90, True),
    "a" : KeyStroke(65, False),
    "b" : KeyStroke(66, False),
    "c" : KeyStroke(67, False),
    "d" : KeyStroke(68, False),
    "e" : KeyStroke(69, False),
    "f" : KeyStroke(70, False),
    "g" : KeyStroke(71, False),
    "h" : KeyStroke(72, False),
    "i" : KeyStroke(73, False),
    "j" : KeyStroke(74, False),
    "k" : KeyStroke(75, False),
    "l" : KeyStroke(76, False),
    "m" : KeyStroke(77, False),
    "n" : KeyStroke(78, False),
    "o" : KeyStroke(79, False),
    "p" : KeyStroke(80, False),
    "q" : KeyStroke(81, False),
    "r" : KeyStroke(82, False),
    "s" : KeyStroke(83, False),
    "t" : KeyStroke(84, False),
    "u" : KeyStroke(85, False),
    "v" : KeyStroke(86, False),
    "w" : KeyStroke(87, False),
    "x" : KeyStroke(88, False),
    "y" : KeyStroke(89, False),
    "z" : KeyStroke(90, False),
    "0" : KeyStroke(57, False), # grrrrrr, someone didn't follow javascript
    "1" : KeyStroke(48, False),
    "2" : KeyStroke(49, False),
    "3" : KeyStroke(50, False),
    "4" : KeyStroke(51, False),
    "5" : KeyStroke(52, False),
    "6" : KeyStroke(53, False),
    "7" : KeyStroke(54, False),
    "8" : KeyStroke(55, False),
    "9" : KeyStroke(56, False),
    "!" : KeyStroke(49, True),
    "@" : KeyStroke(50, True),
    "#" : KeyStroke(51, True),
    "$" : KeyStroke(52, True),
    "%" : KeyStroke(53, True),
    "^" : KeyStroke(54, True),
    "&" : KeyStroke(55, True),
    "*" : KeyStroke(56, True),
    "(" : KeyStroke(57, True),
    ")" : KeyStroke(48, True), # Who the hell knows why this is cyclically shifted, but what do I know...
    "num0" : KeyStroke(96, False),
    "num1" : KeyStroke(97, False),
    "num2" : KeyStroke(98, False),
    "num3" : KeyStroke(99, False),
    "num4" : KeyStroke(100, False),
    "num5" : KeyStroke(101, False),
    "num6" : KeyStroke(102, False),
    "num7" : KeyStroke(103, False),
    "num8" : KeyStroke(104, False),
    "num9" : KeyStroke(105, False),
    "shift" : KeyStroke(160, False),
    "ctrl" : KeyStroke(162, False),
    "alt" : KeyStroke(164, False),
    "tab" : KeyStroke(9, False),
    "\t" : KeyStroke(9, False),
    "space" : KeyStroke(32, False),
    " " : KeyStroke(32, False),
    "enter" : KeyStroke(13, False),
    "\n" : KeyStroke(13,False),
    "-" : KeyStroke(189, False), 
    "=" : KeyStroke(187, False),
    "_" : KeyStroke(189, True),
    "+" : KeyStroke(187, True),
    "[" : KeyStroke(219,False),
    "]" : KeyStroke(221,False),
    "\\" : KeyStroke(220,False),
    "{" : KeyStroke(219,True),
    "}" : KeyStroke(221,True),
    "|" : KeyStroke(220,True),
    ";" : KeyStroke(186,False),
    "'" : KeyStroke(222,False),
    ":" : KeyStroke(186,True),
    "\"" : KeyStroke(222,False),
    "," : KeyStroke(188, False),
    "." : KeyStroke(190, False),
    "/" : KeyStroke(191, False),
    "<" : KeyStroke(188, True),
    ">" : KeyStroke(190, True),
    "?" : KeyStroke(191, True),
    "~" : KeyStroke(192, True),
    "`" : KeyStroke(192, False),
    "F1" : KeyStroke(112, False),
    "F2" : KeyStroke(113, False),
    "F3" : KeyStroke(114, False),
    "F4" : KeyStroke(115, False),
    "F5" : KeyStroke(116, False),
    "F6" : KeyStroke(117, False),
    "F7" : KeyStroke(118, False),
    "F8" : KeyStroke(119, False),
    "F9" : KeyStroke(120, False),
    "F10" : KeyStroke(121, False),
    "F11" : KeyStroke(122, False),
    "F12" : KeyStroke(123, False),
    "backspace" : KeyStroke(8, False),
}

class Macro():

    def __init__(self, text, name, version=MACRO_FILE_VERSION, delay=0):
        self.text=text
        self.name=name
        self.version=version
        self.delay=delay
        self._KEY_TYPE=0
        self._DELAY_TYPE=3
        
        self._create()

    def to_file(self, filepath: pathlib.Path):
        with filepath.open("w") as f:
            self._config.write(f)

    def _create(self):
        self._macro_ctr=0
        self._config = configparser.ConfigParser()
        self._config.optionxform=str
        self._config["General"] = self._create_general()
        self._config["Macro"] = self._create_macro_from_text(self.text)

    def _create_general(self):
        return { "Version" : self.version }

    def _create_macro_from_text(self, text):
        self._macro_ctr = 0
        od = OrderedDict()
        od["Name"] = self.name
        l_keystrokes = [key_stroke_dict[character] for character in text]
        od["MacroStepNum"]=0 # make sure it is in the correct location...
        for i, keystroke in enumerate(l_keystrokes):
            self._create_keystroke(od, keystroke)
        od["MacroStepNum"]=self._macro_ctr
        return od
        
    def _create_keystroke(self, d_macros, keystroke):
        if keystroke.shift_pressed:
            self._create_key_press(d_macros, key_stroke_dict["shift"].keycode, down)
            self._create_delay(d_macros)
            self._create_key_press(d_macros, keystroke.keycode, down)
            self._create_delay(d_macros)
            self._create_key_press(d_macros, keystroke.keycode, up)
            self._create_delay(d_macros)
            self._create_key_press(d_macros, key_stroke_dict["shift"].keycode, up)
            self._create_delay(d_macros)
        else:
            self._create_key_press(d_macros, keystroke.keycode, down)
            self._create_delay(d_macros)
            self._create_key_press(d_macros, keystroke.keycode, up)
            self._create_delay(d_macros)

    def _create_key_press(self, d_macros, keycode, keystate):
        key = "MacroStep{ctr}".format(ctr=self._macro_ctr)
        value = "{type},{keycode},{keystate}".format(type=self._KEY_TYPE, keycode=keycode, keystate=keystate)
        d_macros[key]=value
        self._macro_ctr += 1

    def _create_delay(self, d_macros):
        key = "MacroStep{ctr}".format(ctr=self._macro_ctr)
        value = "{type},{delay}".format(type=self._DELAY_TYPE, delay=self.delay)
        d_macros[key]=value
        self._macro_ctr += 1

#todo... Finish the reader...
def read_macro_file(macro_file_name: pathlib.Path):
    self._config = configparser.ConfigParser()
    with macro_file_name.open() as f:
        self._config.read_file(f)
    
    if (not self._config):
        raise ValueError("Error: no data found in file...")

    return self._config

def parse_args():
    parser = argparse.ArgumentParser("Writes a macro file out...")
    parser.add_argument("--text")
    parser.add_argument("--name")
    parser.add_argument("--filename", required=True)
    parser.add_argument("--read")
    return parser.parse_args()

def main():
    args = parse_args()
    m = Macro(args.text, args.name)
    m.to_file(pathlib.Path(args.filename))

if __name__ == "__main__":
    main()